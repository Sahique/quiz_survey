// JSON2Schema
// File for ID 'template_id': jsoneditor_app/tpl/template4json_tpl.js
// created with JSON2Schema: https://niehausbert.gitlab.io/JSON2Schema

vDataJSON.tpl.template4json =  `

  <!-- Object: root --> 
*  {{{Question}}}
 <!-- String Format: text --> 
*  {{{Option_Type}}}
 <!-- String Format: text --> 
*  
  <!-- Array: root.Options --> 
<!-- Array Path: root.Options  -->
  <!-- Array: root.Options --> 
*  {{{Correct_Answer}}}
 <!-- String Format: text --> 
*  {{{QR_value}}}
 <!-- String Format: text --> 
  <!-- Object: root -->`;
