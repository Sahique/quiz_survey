var QQ=[],survey=[]; 
var surName="";
document.getElementById('inputfile') 
    .addEventListener('change', function() { 
    str=document.getElementById('inputfile').value;  
    surName=str.slice(12,(str.length));console.log(surName);
    var fr=new FileReader(); 
    fr.onload=function(){ 
    QQ=document.getElementById('output').textContent=fr.result; 
    console.log(QQ);
    survey=JSON.parse(QQ); console.log(survey);
    } 
    fr.readAsText(this.files[0]); 
})

function load_survey(L_survey){
    //clearLocalStorage();
    var y1=JSON.parse(localStorage.getItem("final_result"));
   // console.log(y1);
    Survey
        .StylesManager
        .applyTheme("modern");
        console.log("length: "+ survey.questions.length);
        var json = {questions:[]};
        for (i=0;i<L_survey.questions.length;i++){json.questions.push(L_survey.questions[i]); console.log(json);}    

    window.survey = new Survey.Model(json);

    survey
        .onComplete
        .add(function (result) {
            document
                .querySelector('#surveyResult')
                .textContent = "Result JSON:\n" + JSON.stringify(result.data, null, 3);
            
                storeFinalResults(surName,result.data);
        });

    $("#surveyElement").Survey({model: survey});

}

function storeFinalResults(name,answers){
    var st_flag=0;
    var  y=[];
    var obj1={
        survey_name:name,
        survey_results:[]
    }
    y=JSON.parse(localStorage.getItem("final_result"));
    if(y!=null){
        for (i=0;i<y.length;i++){
            if (y[i].survey_name==obj1.survey_name){
                y[i].survey_results.push(answers);
                st_flag=1;
            }
        }
    }
    else{
        y=[ {
                survey_name:name,
                survey_results:[answers] 
            }
        ];
        //y.survey_results.push(answers);
        st_flag=1;
    }
    if(st_flag==0){
        obj1.survey_results.push(answers);
        y.push(obj1);
    }
    localStorage.setItem("final_result",JSON.stringify(y));
    st_flag=0
}
   

function exportFinalResults(){
    var y=JSON.parse(localStorage.getItem("final_result"));
    console.log(y);
    blob = new Blob([JSON.stringify(y)], {type: "text/json;charset=utf-8"});
    saveAs(blob,"_sdat.json");
}

function clearLocalStorage(){
    var r1= confirm("Are you sure to clear the localStorage ?");
    if(r1 == ture){localStorage.clear();}
    else{console.log("You presssed Cancel !")}
    
}